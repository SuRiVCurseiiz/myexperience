package me.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Chronometer;
import android.widget.Toast;
import me.function.RecordAudio;

public class LocalService extends Service {
	// Binder given to clients
	public Chronometer mChronometer;
	private final IBinder serviceBinder = new LocalBinder();
	private RecordAudio ra = new RecordAudio();
	//private AudioPlayer ap = new AudioPlayer();

	private long timesOnChrometer;
	private long timesOnStop;

	public class LocalBinder extends Binder {
		public LocalService getService() {
			// Return this instance of LocalService so clients can call public
			// methods
			resetTimesChrometer();
			
			return LocalService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "On onBind Service ",
				Toast.LENGTH_SHORT).show();
		return serviceBinder;
		
	}

	// ========// Control RecordAudio //=================
	private void setAudioName(String audioName) {
		ra.setAudioName(audioName);
	}

	public void switchRecord(boolean start) {
		if (start){
			setAudioName(mChronometer.getText().toString());
			System.out.println("Status onRecord = " + start);
		}else{
			System.out.println("Save File Record");
		}
		onChronometer(start);
		ra.setOnOffRecord(start);
	}

	//public void onAudio(boolean start) {
	//	ap.onPlay(start);
	//}

	public void setChronometer(Chronometer chronometer) {
		mChronometer = chronometer;
	}

	private void resetTimesChrometer() {
		timesOnChrometer = SystemClock.elapsedRealtime();
		timesOnStop = SystemClock.elapsedRealtime();
	}

	private void onChronometer(boolean status) {
		if (status) {
			timesOnChrometer = timesOnChrometer
					- (timesOnStop - SystemClock.elapsedRealtime());
			mChronometer.setBase(timesOnChrometer);
			mChronometer.start();
			System.out.println("Chronometer  Runing");
		} else {
			timesOnStop = SystemClock.elapsedRealtime();
			System.out.println("Chronometer Stop");
			mChronometer.stop();
		}

	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Toast.makeText(this, "On Service Create ",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Toast.makeText(this, "On Service Destroy ",
				Toast.LENGTH_LONG).show();
	}


	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "On Service Unbind = "+super.onUnbind(intent),
				Toast.LENGTH_LONG).show();
		return super.onUnbind(intent);

	}

}
