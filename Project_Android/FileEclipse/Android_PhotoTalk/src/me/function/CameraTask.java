package me.function;
import static me.data.FileManager.*;

import android.app.AlertDialog;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.util.Log;
import android.widget.FrameLayout;


public class CameraTask {
	private static final String TAG = "Camera Test";
    private Preview mPreview;
    private Camera mCamera;
    private boolean mCheckCamera = false;
    private int numberOfCameras;
    private int cameraCurrentlyLocked;
    private int defaultCameraId;
    
    public void showCamera(Context context,FrameLayout frameLayout) {
        mPreview = new Preview(context);
        frameLayout.addView(mPreview);
        numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
        	Camera.getCameraInfo(i, cameraInfo);
        	if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
        		defaultCameraId = i;
        	}
        }
        mCheckCamera = true;
    }
    
    public boolean getStatusViewCamera(){
    	return mCheckCamera;
    }
    
    public void takePicture(){
    	mPreview.mCamera.takePicture(null, null, jpegCallback);
    }
    
    ShutterCallback shutterCallback = new ShutterCallback() {
		@Override
		public void onShutter() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onShutter'd");
		}
	};
    
    PictureCallback rawCallback = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onPictureTaken - raw");
		}
	};
    
	PictureCallback jpegCallback = new PictureCallback() {
		
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// TODO Auto-generated method stub
			pictureSave(data);
			Log.d(TAG, "onPictureTaken - jpeg");
		}
	};

	public void createCamera(){
		 mCamera = Camera.open();
	     cameraCurrentlyLocked = defaultCameraId;
	     mPreview.setCamera(mCamera);
		
	}
	
    public void destroyCamera() {
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
    }
    
    public void switchCamera(Context context){
    	if (numberOfCameras == 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Device has only one camera!")
                   .setNeutralButton("Close", null);
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }
        if (mCamera != null) {
            mCamera.stopPreview();
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
        mCamera = Camera
                .open((cameraCurrentlyLocked + 1) % numberOfCameras);
        cameraCurrentlyLocked = (cameraCurrentlyLocked + 1)
                % numberOfCameras;
        mPreview.switchCamera(mCamera);
        mCamera.startPreview();
        return;
    }
    
   
}
