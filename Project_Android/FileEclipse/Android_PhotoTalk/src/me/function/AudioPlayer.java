/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" />
 *
 */
package me.function;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.widget.SeekBar;

public class AudioPlayer{
	
	private static final String LOG_TAG = "AudioPlayer";
	private MediaPlayer mPlayer = null;
	private SeekBar seekbar; 
	private File mediaPlayerFile;
	private int maxTime;
	
	private final Handler handler = new Handler();

	public AudioPlayer(SeekBar seekbar,File mediaPlayerFile){
		this.seekbar = seekbar;
		this.mediaPlayerFile = mediaPlayerFile;
		
	}
	public void onPlay(boolean start) {
		if (start) {
			startPlaying();
		} else {
			pausePlaying();
		}
	}

	private void startPlaying() {
		if(mPlayer == null){
			this.mPlayer = new MediaPlayer();
			try {
				mPlayer.setDataSource(mediaPlayerFile.toString());
				mPlayer.prepare();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.maxTime = mPlayer.getDuration();
			seekbar.setMax(maxTime);
		}
		mPlayer.start();
		startPlayProcessUpdate();
			
	}
	
	public void setPosition(int msec){
		mPlayer.seekTo(msec);
	}
	
	public boolean isStop(){
		if(mPlayer==null){
			return false;
		}
		return true;
	}
	
	private void startPlayProcessUpdate() {
		if (mPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
				@Override
				public void run() {	
					startPlayProcessUpdate();
					seekbar.setProgress(mPlayer.getCurrentPosition());
				}
			};
			handler.postDelayed(notification, 1000);
		} else if(seekbar.getProgress() < seekbar.getMax()){
			pausePlaying();
		}else {
			stopPlaying();
			Log.d(LOG_TAG, "mPlayer Stop");
		}
	}

	public void stopPlaying() {
		mPlayer.stop();
		mPlayer.release();
		mPlayer = null;
	}
	
	public void pausePlaying(){
		mPlayer.pause();
	}
	
	

}