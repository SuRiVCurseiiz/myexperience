package me.function;
import static me.data.ConfigMysqlHelper.*;
import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

@SuppressWarnings("deprecation")
public class UploadAudio extends AsyncTask<String, Void, Void> {
	String file_upload_URL = getHost()+UPLOAD_AUDIO;
	public UploadAudio (String filePath,String project_ID){
		super();
		this.execute(filePath,file_upload_URL,project_ID);	
	}
	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		this.uploadFile(params[0],params[1],params[2]);
		return null;
	}
	
     private String uploadFile(String filePath,String file_upload_URL,String project_ID) {
         String responseString = null;

         HttpClient httpclient = new DefaultHttpClient();
         HttpPost httppost = new HttpPost(file_upload_URL);

         try {
             MultipartEntity entity = new MultipartEntity();

             File sourceFile = new File(filePath);

             // Adding file data to http body
             entity.addPart("audioFile", new FileBody(sourceFile));
             entity.addPart("user_ID", new StringBody(project_ID));

             httppost.setEntity(entity);

             // Making server call
             HttpResponse response = httpclient.execute(httppost);
             HttpEntity r_entity = response.getEntity();

             int statusCode = response.getStatusLine().getStatusCode();
             if (statusCode == 200) {
                 // Server response
                 responseString = EntityUtils.toString(r_entity);
             } else {
                 responseString = "Error occurred! Http Status Code: "
                         + statusCode;
             }

         } catch (ClientProtocolException e) {
             responseString = e.toString();
         } catch (IOException e) {
             responseString = e.toString();
         }

         return responseString;

     }

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}
	 
	 

}
