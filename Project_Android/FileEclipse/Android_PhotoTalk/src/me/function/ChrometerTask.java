package me.function;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.Chronometer;

public class ChrometerTask extends Chronometer {

	public ChrometerTask(Context context) {
		super(context);
		resetTimesChrometer();
	}

	public ChrometerTask(Context context, AttributeSet attrs) {
		super(context, attrs);
		resetTimesChrometer();
	}

	public ChrometerTask(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		resetTimesChrometer();
	}

	public long timesOnChrometer;
	public long timesOnStop;

	public void resetTimesChrometer() {
		timesOnChrometer = SystemClock.elapsedRealtime();
		timesOnStop = SystemClock.elapsedRealtime();
	}

	@Override
	public void start() {
		super.start();
		timesOnChrometer = timesOnChrometer
				- (timesOnStop - SystemClock.elapsedRealtime());
		this.setBase(timesOnChrometer);
	}

	@Override
	public void stop() {
		super.stop();
		timesOnStop = SystemClock.elapsedRealtime();
	}


}
