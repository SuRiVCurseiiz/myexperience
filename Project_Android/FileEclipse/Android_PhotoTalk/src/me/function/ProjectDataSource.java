package me.function;

import java.util.ArrayList;
import java.util.List;

import me.data.Projects;

import com.me.lib.SqliteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ProjectDataSource {
	private SQLiteDatabase database;
	private SqliteHelper dbHelper;
	private String[] allProjects = { SqliteHelper.COLUMN_ID,
			SqliteHelper.COLUMN_NAME, SqliteHelper.COLUMN_TIME,
			SqliteHelper.COLUMN_DATE, SqliteHelper.COLUMN_PICTURE };

	public ProjectDataSource(Context context) {
		dbHelper = new SqliteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void inseartProject(int id,String name, String tmp, String times,
			String dates) {
		ContentValues content = new ContentValues();
		content.put(SqliteHelper.COLUMN_ID, id);
		content.put(SqliteHelper.COLUMN_NAME, name);
		content.put(SqliteHelper.COLUMN_TIME, times);
		content.put(SqliteHelper.COLUMN_DATE, dates);
		content.put(SqliteHelper.COLUMN_PICTURE, tmp);

		String table = SqliteHelper.TABLE_PROJECT;

		try {
			database.insert(table, null, content);
		} catch (NullPointerException nException) {
			System.out.println(content);
			nException.printStackTrace();
		}

	}

	public void deleteProject(long id) {
		System.out.println("Comment deleted with id: " + id);
		database.delete(SqliteHelper.TABLE_PROJECT, SqliteHelper.COLUMN_ID
				+ " = " + id, null);
	}

	public List<Projects> getAllProjects() {
		List<Projects> projects = new ArrayList<Projects>();

		Cursor cursor = database.query(SqliteHelper.TABLE_PROJECT, allProjects,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			projects.add(new Projects(cursor.getLong(0),
									cursor.getString(1),
									cursor.getString(2),
									cursor.getString(3),
									cursor.getString(4)));
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return projects;
	}

}
