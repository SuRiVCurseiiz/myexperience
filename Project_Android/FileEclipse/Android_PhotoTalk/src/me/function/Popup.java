package me.function;

import static me.data.FileManager.*;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

public class Popup {
	private static String fileName;
	public static String showPopUpAndSaveFile(Context context,String oldFileName){
		fileName = oldFileName;
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		
		alert.setTitle("Save File");
		alert.setMessage("please input name file");
		
		// Set an EditText view to get user input
		final EditText input = new EditText(context);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int whichButton) {
			fileName = input.getText().toString();
			renameFile(fileName);
			// Do something with value!
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  @Override
		public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});
		
		alert.show();
		return fileName;
		// see http://www.androidsnippets.com/prompt-user-input-with-an-alertdialog
	}
}
