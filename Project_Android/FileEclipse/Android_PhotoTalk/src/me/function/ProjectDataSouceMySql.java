package me.function;
import static me.data.ConfigMysqlHelper.*;
import java.util.HashMap;

import android.util.Log;

import com.me.lib.MySqlDatabaseHelper;


public class ProjectDataSouceMySql {
	private String userID;
	private String projectName;
	private HashMap<String, Object> mapData;
	private MySqlDatabaseHelper mySqlHelper;
	
	public ProjectDataSouceMySql(String userID,String projectName) {
		this.userID = userID;
		this.projectName = projectName;
		mySqlHelper = new MySqlDatabaseHelper(getProjectData());
	}

	private HashMap<String, Object> getProjectData(){
		mapData = new HashMap<String, Object>();
		mapData.put("userID", userID);
		mapData.put("projectName", projectName);
		return mapData;
	}
	
	public void insertToMySql(){
		mySqlHelper.chainToServerPhP(INSERT_PROJECT);
	}
	
	public int getIDProject(String userID){
		int idProject = 0;
		String key = "project_id";
		mySqlHelper.chainToServerPhP(SELECT_PROJECT_ID);
		idProject =  (Integer) mySqlHelper.getReturn(key);
		
		return idProject;
	}
	
	public void updateProjectName(int projectID,String projectNameEdit){
		mySqlHelper.putValue("projectID", projectID);
		mySqlHelper.putValue("projectNameEdit", projectNameEdit);
		mySqlHelper.chainToServerPhP(UPDATE_PROJECT_NAME);
	}

}
