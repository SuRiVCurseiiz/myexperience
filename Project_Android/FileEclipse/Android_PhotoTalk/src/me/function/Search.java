package me.function;
public class Search {
	private int arr[];
	private int checkSize;
	private int size;
	
	public Search(int arr[]){
		this.arr = arr;
		size = arr.length-1;
	}
	
	/**
	 * Search Binary nearly index
	 * 
	 * @param value
	 *         value for check nearly index in array  
	 *            
	 * @return number of array index 
	 */
	public int searchBinary_nearly(int value){
		checkSize = size;
		int in = checkSize/2;
		checkSize = checkSize / 2;
		return searchBinary_nearly(value ,in);
	}
	
	private int searchBinary_nearly(int value,int in){
		int k = arr[in];
		
		if(arr.length == 0){
			return -1;
		}
		
		if(in == size || in == 0 || k==value){
			return in;
		}
		
		if(checkSize == 0){
			if(k>value){
				return in-1;
			}else{
				return in;
			}
		}
		checkSize = checkSize / 2;
		if(k < value){
			int k2 = arr[in+1];
			if(k2 > value){
				return in;
			}else{						
				in = in + (checkSize);
				return searchBinary_nearly(value , in);
			}
		}else{
			int k2 = arr[in-1];
			if(k2 <= value){
				return (in-1);
			}else{
				if(checkSize == 0){
					checkSize = 1;
				}
				in = in - checkSize;
				return searchBinary_nearly(value , in);
			}
		}
	}
	
	public int searchFor_nearly(int value){
		int i=0;
		for(int x:arr){
			if(x>value){
				return 0;
			}else if(x<=value){
				return i;
			}
			i++;			
		}
		return size;
	}
	
}
