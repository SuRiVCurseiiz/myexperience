package me.function;
import static me.data.ConfigMysqlHelper.*;

import java.util.HashMap;
import com.me.lib.MySqlDatabaseHelper;

public class UploadDataImgSql {
	String image_str;
	private MySqlDatabaseHelper mySqlHelper;
	private HashMap<String, Object> map = new HashMap<String, Object>(); 
	
	public UploadDataImgSql(byte []data) {
		map = encodeImageToString(data);
	}

	private HashMap<String, Object> encodeImageToString(byte []imgdata) {
		//image_str = Base64.encodeBytes(imgdata);
		 HashMap<String ,Object> map = new HashMap<String, Object>();
		 map.put("imageJson", imgdata);
		return map;
	}
	
	public void insertimageDataToSql(){
		mySqlHelper = new MySqlDatabaseHelper(map);
		mySqlHelper.chainToServerPhP(INSERT_IMAGE);
	}

}
