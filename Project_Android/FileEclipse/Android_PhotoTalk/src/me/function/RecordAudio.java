/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" />
 *
 */
package me.function;
import android.os.Handler;
import android.util.Log;

public class RecordAudio{
	private static final String LOG_TAG = "RecordTest";
	private RecMicToMp3 mRecorder = null;
	boolean mRecordingStatus = true;
	private static String audioName;
	
	public RecordAudio(){}
	public RecordAudio(String mFilePath){
		mRecorder = new RecMicToMp3(mFilePath, 8000);
	}

		
	public void setOnOffRecord(boolean start) {
		if (start) {
			Log.e(LOG_TAG, "Start Record");
			startRecording();
		} else {
			Log.e(LOG_TAG, "Pause Record");
			pauseRecording();
		}
	}
	
	public void setAudioName(String audioName){
		RecordAudio.audioName = audioName;
	}
	
	private void startRecording() {
		mRecorder.start();
		
	}

	public void stopRecording() {
		System.out.println("Print :: "+audioName+" is stopRecord");
		pauseRecording();
		
	}

	private void pauseRecording() {
			mRecorder.stop();
	}
	
	public void setHandle(Handler handler) {
		mRecorder.setHandle(handler);
	}
	
	
}