package me.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Projects implements Parcelable{
	private long id;
	private String name;
	private String times;
	private String dates;
	private String picture;
	
	public Projects(){
		
	}
  
	public Projects(long id, String name, String times, String dates,
			String picture) {
		this.id = id;
		this.name = name;
		this.times = times;
		this.dates = dates;
		this.picture = picture;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTimes() {
		return times;
	}
	public void setTimes(String times) {
		this.times = times;
	}
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Projects(Parcel in){
		this.id = in.readLong();
		this.name = in.readString();
		this.times = in.readString();
		this.dates = in.readString();
		this.picture = in.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(times);
		dest.writeString(dates);
		dest.writeString(picture);
	}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
		public Projects createFromParcel(Parcel in) {
            return new Projects(in); 
        }

        @Override
		public Projects[] newArray(int size) {
            return new Projects[size];
        }
    };

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ID : "+id+"\n"+
				"Name : "+name+"\n"+
				"Time : "+times+"\n"+
				"Date : "+dates+"\n";
	}
    
    
	
}
