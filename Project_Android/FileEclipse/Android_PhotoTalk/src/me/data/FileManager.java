package me.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import me.function.UploadDataImgSql;

import android.os.Environment;
import android.util.Log;

public class FileManager {
	//private static final String TIME_MILLIS = System.currentTimeMillis() + "";
	private static final String MAIN_DIRECTORY = "Photos_Talk";
	private static String DIRECTORY_PROJECT;
	private static final String AUDIO_DIRECTORY = "Audio";
	private static final String PHOTO_DIRECTORY = "Photo";
	private static final String AUDIO_TYPE = ".mp3";
	private static final String PICTURE_TYPE_JPG = ".jpg";
	
	private static final String ROOT_PATH = Environment
			.getExternalStorageDirectory().getAbsolutePath() + "/";
	private static final String MAIN_PATH = ROOT_PATH + MAIN_DIRECTORY + "/";
	
	private static String fileRecordName;
	private static String PictureNAME;
	
	private static File FileManager_file;
	private static File FileManager_fileList[];
	
	private static String DIRECTORY;
	
	protected static LinkedList<Picture> LIST_PICTURE = new LinkedList<Picture>();
	protected static Picture objectPic = new Picture();
	
	private static UploadDataImgSql uploadImage;

	public static String getDirectoryProjectName(){
		return DIRECTORY_PROJECT;
	}
	
	public static void setFileRecordName(String fileRecordName) {
		FileManager.fileRecordName = fileRecordName;
	}

	public static void setThisPictureName(String thisPictureName) {
		FileManager.PictureNAME = thisPictureName;
	}

	public static String getStorgeAndNameRecordFile() {
		return getDirectoryRecordFile() + getFileRecord();
	}

	public static boolean createMainDirectory() {
		return createDirIfNotExists(MAIN_DIRECTORY);
	}

	public static boolean createTempDirectory() {
		return createDirIfNotExists(MAIN_DIRECTORY + "/" + DIRECTORY_PROJECT);
	}

	public static boolean createAudioDirectory() {
		return createDirIfNotExists(MAIN_DIRECTORY + "/" + DIRECTORY_PROJECT + "/"
				+ AUDIO_DIRECTORY);
	}

	public static boolean createPhotoDirectory() {
		// TODO Auto-generated method stub
		return createDirIfNotExists(MAIN_DIRECTORY + "/" + DIRECTORY_PROJECT + "/"
				+ PHOTO_DIRECTORY);
	}

	public static boolean createDirIfNotExists(String name) {
		// folder = System.currentTimeMillis()+"";
		DIRECTORY = name;
		FileManager_file = new File(getDirectoryPath());
		if (!FileManager_file.exists()) {
			if (!FileManager_file.mkdirs()) {
				Log.e("TravellerLog :: ", "Problem creating " + name
						+ " folder");
			}
			return false;
		}
		return true;
	}
	
	public static void createDirectory() {
		createMainDirectory();
		createTempDirectory();
		createPhotoDirectory();
		createAudioDirectory();
	}

	private static String getFileRecord() {
		return fileRecordName + AUDIO_TYPE;
	}

	public static String getDirectoryPath() {
		return ROOT_PATH + DIRECTORY;
	}
	
	public static void setDirectoryProject(String name){
		DIRECTORY_PROJECT = name;
	}
	
	public static String getDirectoryProject(){
		return getMainDirectoryPath()+DIRECTORY_PROJECT+"/";
	}
	
	public static String getMainDirectoryPath() {
		return MAIN_PATH;
	}

	public static String getDirectoryRecordFile() {
		return getDirectoryProject() + AUDIO_DIRECTORY + "/";
	}

	public static String getDirectoryPicture() {
		// TODO Auto-generated method stub
		return getDirectoryProject() + PHOTO_DIRECTORY + "/";
	}

	public static String getPictureName() {
		return PictureNAME + PICTURE_TYPE_JPG;
	}
	
	public static FileInputStream getFile(String path,String filename){
		FileInputStream file = null;
		try {
			file = new FileInputStream(path+"/"+filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}

	public static File[] getAllFile(File directory) {
		if(!directory.isDirectory()){
			return null;
		}
		FileManager_fileList = directory.listFiles();
		Log.d("File", FileManager_fileList[0].toString());
		return FileManager_fileList;
	}

	// ================= This Area for Camera function ==================//
	public static boolean pictureSave(byte[] data) {
		FileOutputStream outStream = null;
		String picture = getPictureName();
		try {
			outStream = new FileOutputStream(
					String.format(getDirectoryPicture() + "/"
							+ picture));
			addPictureToList(new Picture(picture, data));
				
			outStream.write(data);
			uploadImage = new UploadDataImgSql(data);
			uploadImage.insertimageDataToSql();
			outStream.close();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch(NullPointerException ne){
			ne.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static LinkedList<Picture> getPictureList(){
		return LIST_PICTURE;
	} 

	private static void addPictureToList(Picture pic){
		LIST_PICTURE.add(pic);
	}

	public static void saveThisProject(String newname){
		renameFile(newname);
	}
	
	public static boolean renameFile(String newname) {
		boolean success = false;
		success = renameFile(DIRECTORY_PROJECT,newname);
		return success;
	}
	
	public static boolean renameFile(String name, String newname) {
		boolean success = false;
		File file = new File(getMainDirectoryPath() + name);
		File file2 = new File(getMainDirectoryPath() + newname);
		success = file.renameTo(file2);
		return success;
	}
	
	
	private static void deleteAudioRaw(){
		File delFile = new File(getDirectoryRecordFile(), getFileRecord());
		delFile.delete();
	}
	
	public static void mergeFileAudio(){
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream(new File(getDirectoryRecordFile(), getFileRecord()));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mergeFileAudio(fileIn);
		
	}
	public static void mergeFileAudio(FileInputStream file){
		
		File newFile = new File(getDirectoryRecordFile() ,"record.mp3");

		if (!newFile.exists()){
			try {
				newFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {
			FileOutputStream fostream = new FileOutputStream(newFile,
					true);	
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int temp=0;
			while ((temp = file.read(buffer)) != -1) {
				fostream.write(buffer,0,temp); // to write to file
				
			}
			fostream.close();
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			deleteAudioRaw();
		}
	}

}
