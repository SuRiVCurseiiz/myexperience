package me.data;

public class Picture {
	private String picture;
	private byte[] picbyte;

	public String getName() {
		return picture;
	}
	
	public Picture(String picname,byte[] picbyte){
		this.picture = picname;
		this.picbyte = picbyte;
	}
	
	public Picture(){
		
	}
	
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public byte[] getPicbyte() {
		return picbyte;
	}

	public void setPicbyte(byte[] picbyte) {
		this.picbyte = picbyte;
	}
	
}
