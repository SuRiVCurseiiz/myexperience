package com.me.lib;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import me.data.ConfigMysqlHelper;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MySqlDatabaseHelper {
	private AsyncHttpClient client;
	private RequestParams params;
	private String value;
	private HashMap<String, Object> valueHash;
	private JSONArray arr;
	private Gson gson;
	private String returnProjectID = new String();
	private HashMap<String, Object> returns = new HashMap<String, Object>();
	private static boolean isfinish = false;

	public void setIsfinish(boolean isfinish) {
		this.isfinish = isfinish;
	}

	public boolean isFinish() {
		return isfinish;
	}

	public MySqlDatabaseHelper(HashMap<String, Object> value) {
		valueHash = value;
	}

	public String hashMapToJson(HashMap<String, Object> value) {
		// Use GSON to serialize Array List to JSON
		gson = new GsonBuilder().create();
		return gson.toJson(value);
	}

	public void putValue(String key, Object obj) {
		valueHash.put(key, obj);
	}

	public void chainToServerPhP(String filePhP) {
		setIsfinish(false);
		client = new AsyncHttpClient();
		params = new RequestParams();
		String url = getHost() + filePhP;
		this.value = hashMapToJson(valueHash);
		params.put("value", value);
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				super.onFinish();
				setIsfinish(true);
				
			}

			@Override
			@Deprecated
			public void onFailure(int statusCode, Throwable error,
					String content) {
				// TODO Auto-generated method stub
				super.onFailure(statusCode, error, content);
				if (statusCode == 404) {
					System.out.println("Requested resource not found");
				} else if (statusCode == 500) {
					System.out.println("Something went wrong at server end");
				} else {
					System.out
							.println("Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]");
				}
			}

			@Override
			@Deprecated
			public void onSuccess(String content) {
				// TODO Auto-generated method stub
				super.onSuccess(content);
				try {
					System.out.println(content);
					arr = new JSONArray(content);
					JSONObject obj = (JSONObject) arr.get(0);
					setReturns(getResponse(obj));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private HashMap<String, Object> getResponse(JSONObject response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		Iterator keys = response.keys();

		while (keys.hasNext()) {
			
			String key = (String) keys.next();
			try {
				returns.put(key, response.get(key).toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return map;
	}
	
	public void setReturns(HashMap<String, Object> map){
		//returns = map;
	}

	public Object getReturn(Object key) {
	
		System.out.println(returns);
		return 0;
		
	}

	public String getHost() {
		return ConfigMysqlHelper.getHost();
	}

}
