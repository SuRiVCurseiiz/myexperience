package com.me.Activity;

import static me.data.FileManager.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import me.data.Picture;
import me.function.CameraTask;
import me.function.ProjectDataSource;
import me.function.RecMicToMp3;
import me.function.RecordAudio;
import me.function.ProjectDataSouceMySql;
import me.function.UploadAudio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.me.phototalk.R;

public class MainActivity extends Activity {

	private CameraTask cameraTask = new CameraTask();
	private RecordAudio record;
	private boolean recordNow = false;
	private Handler recordHandler;

	private FrameLayout frameLayout;
	private ImageButton imBtnRecord;
	private ImageButton imBtnSave;
	private ImageButton imBtnCapture;

	private final Context context = this;

	private Date dateNow;
	private SimpleDateFormat ft;
	private long timesOnChrometer;
	private long timesOnStop;
	private Chronometer chroMeterTimes;

	private List<Bitmap> bitMapList = new ArrayList<Bitmap>();
	private LinkedList<Picture> pictureList;

	private String userID;
	private int projectID;
	private ProjectDataSouceMySql projectMySql;
	private ProjectDataSource datasource;

	public Intent intentService;

	@SuppressLint({ "SimpleDateFormat", "HandlerLeak" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_main);
			dateNow = new Date();
			ft = new SimpleDateFormat("yyyy.MM.dd");
			datasource = new ProjectDataSource(context);
			datasource.open();
			Bundle extra = getIntent().getExtras();
			userID = extra.getString("UserID");
			System.out.println(userID);
			frameLayout = ((FrameLayout) findViewById(R.id.frameLayout1));
			cameraTask.showCamera(getApplicationContext(), frameLayout);
			imBtnCapture = (ImageButton) findViewById(R.id.imBtnCapture);
			imBtnCapture.setOnClickListener(imBtnCaptureListener);

			record = new RecordAudio();
			chroMeterTimes = (Chronometer) findViewById(R.id.cTimes);
			pictureList = getPictureList();
			imBtnSave = (ImageButton) findViewById(R.id.imBtnSave);
			imBtnSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDialogSave();
				}
			});

			imBtnRecord = (ImageButton) findViewById(R.id.imBtnRecord);
			imBtnRecord.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Start // Stop Record
					recordChange();
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		recordHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case RecMicToMp3.MSG_REC_STARTED:

					break;
				case RecMicToMp3.MSG_REC_STOPPED:

					break;

				case RecMicToMp3.MSG_ERROR_GET_MIN_BUFFERSIZE:
					Toast.makeText(MainActivity.this,
							"ERROR_GET_MIN_BUFFERSIZE", Toast.LENGTH_LONG)
							.show();
					break;
				case RecMicToMp3.MSG_ERROR_CREATE_FILE:
					Toast.makeText(MainActivity.this, "Error at Create File",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_REC_START:
					Toast.makeText(MainActivity.this, "MSG ERROR REC START",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_AUDIO_RECORD:
					Toast.makeText(MainActivity.this, "MSG ERROR AUDIO RECORD",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_AUDIO_ENCODE:
					Toast.makeText(MainActivity.this, "MSG ERROR AUDIO ENCODE",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_WRITE_FILE:
					Toast.makeText(MainActivity.this, "MSG ERROR WRITE FILE",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_CLOSE_FILE:
					// Toast.makeText(MainActivity.this, "MSG ERROR CLOSE FILE",
					// Toast.LENGTH_LONG).show();
					break;
				default:
					break;
				}
			}
		};
		// Database MySql
		projectMySql = new ProjectDataSouceMySql(userID,
				getDirectoryProjectName());
		projectMySql.insertToMySql();
	}

	public void recordChange() {
		recordChange(recordNow);
	}

	private int count = 0;

	public void recordChange(boolean recordNow) {
		if (!recordNow) {
			setFileRecordName("audio_raw" + count);
			record = new RecordAudio(getStorgeAndNameRecordFile());

			chroMeterTimes.start();
			timesOnChrometer = timesOnChrometer
					- (timesOnStop - SystemClock.elapsedRealtime());
			chroMeterTimes.setBase(timesOnChrometer);
			recordNow = true;

			// Record Start
			record.setHandle(recordHandler);
			record.setOnOffRecord(recordNow);

		} else {
			recordNow = false;
			timesOnStop = SystemClock.elapsedRealtime();
			chroMeterTimes.stop();
			// Record Stop
			record.setOnOffRecord(recordNow);
			new Thread() {
				@Override
				public void run() {
					android.os.Process
							.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
					mergeFileAudio();
				}
			}.start();
			count++;
		}
		this.recordNow = recordNow;
	}

	public void showDialogSave() {
		boolean recordStop = true;
		recordChange(recordStop);
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_finish);
		dialog.setTitle("Save Project");
		dialog.setCancelable(false);
		// final ImageView img = (ImageView)
		// dialog.findViewById(R.id.img_example_profile);

		final EditText input = (EditText) dialog
				.findViewById(R.id.edittxt_nameproject);
		input.setHint(getDirectoryProjectName());

		dialog.show();
		ImageButton dialogButtonSave = (ImageButton) dialog
				.findViewById(R.id.imBtnSave2);
		dialogButtonSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String nameproject;
				if (input.getText() == null) {
					nameproject = getDirectoryProjectName();
				} else {
					nameproject = input.getText().toString();
				}
				saveThisProject(nameproject);
				new UploadAudio(getDirectoryRecordFile() + "record.mp3", String
						.valueOf(projectID));
				datasource.inseartProject(projectID, nameproject, "picture",
						chroMeterTimes.getText().toString(), ft.format(dateNow));
				dialog.dismiss();
				record.stopRecording();
				onBackPressed();
			}
		});

		ImageButton dialogButtonCancle = (ImageButton) dialog
				.findViewById(R.id.imBtnClose);
		dialogButtonCancle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	public MainActivity() {
		setDirectoryProject("Temp_PhotoTalk" + System.currentTimeMillis() + "");
		createDirectory();
	}

	private long getTimeOnChrometer() {
		return (SystemClock.elapsedRealtime() - chroMeterTimes.getBase()) / 1000;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private OnClickListener imBtnCaptureListener = new OnClickListener() {
		@Override
		public void onClick(final View v) {

			v.setEnabled(false);
			if (cameraTask.getStatusViewCamera() && recordNow) {
				setThisPictureName(String.valueOf(getTimeOnChrometer()));
				try {
					cameraTask.takePicture();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					showExamplePicture();
				}
			}
			// final int resId = imBtnCapture.getId();
			// imBtnCapture.setImageBitmap(bm);
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					v.setEnabled(true);
					// imBtnCapture.setImageResource(resId);
				}
			}, 1000);
		}
	};

	private void resetTimesChrometer() {
		timesOnChrometer = SystemClock.elapsedRealtime();
		timesOnStop = SystemClock.elapsedRealtime();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to LocalService
		resetTimesChrometer();
		chroMeterTimes.setBase(SystemClock.elapsedRealtime());
		projectID = projectMySql.getIDProject(userID);

	}

	@Override
	protected void onStop() {
		super.onStop();
		// Unbind from the service
	}

	@Override
	public void onResume() {
		datasource.open();
		cameraTask.createCamera();
		super.onResume();
	}

	@Override
	public void onPause() {
		cameraTask.destroyCamera();
		datasource.close();
		super.onPause();
		// mService.ra.onPause();
	}

	@Override
	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
		record.setOnOffRecord(false);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	private void clearListView() {
		ListView list = (ListView) findViewById(R.id.LstShowPicture_Tmp);
		list.removeAllViews();
	}

	public void showExamplePicture() {
		try {
			if (bitMapList.size() == 5) {
				clearListView();
				bitMapList.remove(4);
			}

			if (pictureList.isEmpty()) {
				return;
			}
			// FileInputStream newfile =
			// getFile(getDirectoryPicture(),pictureList.getLast().getName());
			// fileList.add(newfile);
			byte[] data = pictureList.getLast().getPicbyte();
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			bitMapList.add(bitmap);

		} catch (IndexOutOfBoundsException indexOutE) {
			indexOutE.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		ArrayAdapter<Bitmap> adapterPicture = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.LstShowPicture_Tmp);
		list.setAdapter(adapterPicture);
	}

	private class MyListAdapter extends ArrayAdapter<Bitmap> {

		public MyListAdapter() {
			super(MainActivity.this, R.layout.list_showpicture_tmp, bitMapList);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			try {
				if (itemView == null) {
					itemView = getLayoutInflater().inflate(
							R.layout.list_showpicture_tmp, parent, false);
				}

				Bitmap bitmapFile = bitMapList.get(position);
				ImageView imageview = (ImageView) findViewById(R.id.imgTmp);
				imageview.setImageBitmap(bitmapFile);
			} catch (Exception E) {
				E.printStackTrace();
			}
			return itemView;
		}

	}

}
