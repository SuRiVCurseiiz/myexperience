package com.me.Activity;

import java.io.File;

import me.data.Projects;
import me.function.AudioPlayer;
import me.function.Search;
import static me.data.FileManager.*;

import com.me.phototalk.R;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ViewSwitcher.ViewFactory;

public class PlayProjectActivity extends Activity {

	private AudioPlayer audio;
	private File mediaPlayerFile;
	private ImageButton btnPlaySwitch;
	private Boolean isPlay = true;
	private SeekBar seekbar;
	private File[] filePic;
	private int pic_Time[];
	private Search searchPic;
	private int checkIndex = 0;
	private ImageSwitcher imageSwtich;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_project);
		Bundle data = getIntent().getExtras();
		Projects projects = (Projects) data.getParcelable("projects");
		setDirectoryProject(projects.getName());
		System.out.println(projects.getName());
		mediaPlayerFile = new File(getDirectoryRecordFile(), "record.mp3");
		btnPlaySwitch = (ImageButton) findViewById(R.id.imgBtnPlay);
		btnPlaySwitch.setOnClickListener(btnPlayClick);
		seekbar = (SeekBar) findViewById(R.id.seekBar1);
		seekbar.setOnSeekBarChangeListener(onSeekBarChange);
		
		audio = new AudioPlayer(seekbar, mediaPlayerFile);

		this.loadFilePicture();
		this.createAlbum();
		this.createImageSwitch();
		
		audio.onPlay(isPlay);
		btnPlayChange(isPlay);
		isPlay = !isPlay;
	}

	private void createImageSwitch() {
		imageSwtich = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
		// Set the ViewFactory of the ImageSwitcher that will create ImageView
		// object when asked
		imageSwtich.setFactory(new ViewFactory() {

			@Override
			public View makeView() {
				// TODO Auto-generated method stub

				// Create a new ImageView set it's properties
				ImageView imageView = new ImageView(getApplicationContext());
				imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
				imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				return imageView;
			}
		});

		// Declare the animations and initialize them
		Animation in = AnimationUtils.loadAnimation(this,
				android.R.anim.slide_in_left);
		Animation out = AnimationUtils.loadAnimation(this,
				android.R.anim.slide_out_right);

		// set the animation type to imageSwitcher
		imageSwtich.setInAnimation(in);
		imageSwtich.setOutAnimation(out);
	}

	private void createAlbum() {
		pic_Time = new int[filePic.length];
		try {
			this.setTimePic();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		searchPic = new Search(pic_Time);
	}

	private int getPicIndex(int value) {
		return searchPic.searchBinary_nearly(value);
	}

	private void loadFilePicture() {
		File f = new File(getDirectoryPicture());
		filePic = getAllFile(f);
	}

	private void setTimePic() {
		int i = 0;
		for (File x : filePic) {
			String str[] = x.getName().split(".jpg");
			pic_Time[i] = Integer.valueOf(str[0]);
			i++;
		}
		System.out.println("Length Pic[] = "+pic_Time.length);
	}

	private void showPicture(int index) {
		System.out.println("Show Pic Index = "+pic_Time[index]);
		imageSwtich.setImageURI(Uri.fromFile(filePic[index]));
		checkIndex++;
	}

	private void showPicture(int progress, boolean fromUser) {
		if(checkIndex >= pic_Time.length){
			return;
		}
		
		if (fromUser) {
			checkIndex = getPicIndex(progress);
			showPicture(checkIndex);
		} else {
			if (progress != pic_Time[checkIndex]) {
				return;
			}
			showPicture(checkIndex);
		}

	}

	private OnClickListener btnPlayClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
				audio.onPlay(isPlay);
				btnPlayChange(isPlay);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			isPlay = !isPlay;
		}
	};

	private void btnPlayChange(boolean isPlay) {
		if (isPlay) {
			btnPlaySwitch.setImageResource(android.R.drawable.ic_media_pause);
		} else {
			btnPlaySwitch.setImageResource(android.R.drawable.ic_media_play);
		}
	}

	private OnSeekBarChangeListener onSeekBarChange = new OnSeekBarChangeListener() {

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			int isProgress;
			if(fromUser){
				if(audio.isStop()){
					audio.onPlay(true);
				}
				audio.setPosition(progress);
			}
			
			if(progress%1000 > 850){
				isProgress = (int)Math.ceil(progress / 1000.0);
			}else{
				isProgress = progress / 1000;
			}
			showPicture(isProgress, fromUser);
			
		}
	};

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		audio.stopPlaying();
	}
	
}
