package com.me.Activity;

import java.util.List;

import me.data.Projects;
import me.function.ProjectDataSource;
import com.me.phototalk.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class StartUpActivity extends Activity {
	Button btnStart, btnTestRecord;
	public FrameLayout frameLayout;
	public Intent intentRecordAndCamera;
	private Context context = this;
	private ProjectDataSource datasource;
	private List<Projects> cursorProjects;
	private ListView listview;
	private String userID = null;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.startup_main);
		btnStart = (Button) findViewById(R.id.bntStart);
		btnStart.setOnClickListener(btnStartIntent);
		Bundle extra = getIntent().getExtras();
		this.setUserID(extra.getString("UserID"));

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private OnClickListener btnStartIntent = new OnClickListener() {
		@Override
		public void onClick(View v) {
			intentRecordAndCamera = new Intent(getBaseContext(), MainActivity.class);
			intentRecordAndCamera.putExtra("UserID", getUserID());
			StartUpActivity.this.startActivity(intentRecordAndCamera);
		}
	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		this.showListProjects();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		datasource.close();
		super.onPause();
	}
	
	public void showListProjects(){
		datasource = new ProjectDataSource(context);
		datasource.open();
		cursorProjects = datasource.getAllProjects();
		ArrayAdapter<Projects> listProjects = new ListProjects(StartUpActivity.this, R.layout.list_showproject, cursorProjects);
		listview = (ListView) findViewById(R.id.listviewProject);
		listview.setAdapter(listProjects);
		listview.setOnItemClickListener(listViewClick);
	}
	
	public OnItemClickListener listViewClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View itemView, int position,	
				long id) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(context,PlayProjectActivity.class);
			intent.putExtra("projects", cursorProjects.get((int)id));
			StartUpActivity.this.startActivity(intent);
		}
		
	};

	private class ListProjects extends ArrayAdapter<Projects> {

		private List<Projects> listProject;
		int viewID;
		private Activity context;

		// private Activity activityContext;

		public ListProjects(Activity context,int textViewResourceId,
				List<Projects> cursorProjects) {
			super(context , textViewResourceId , cursorProjects);
			// TODO Auto-generated constructor stub
			this.context = context;
			viewID = textViewResourceId;
			listProject = cursorProjects;
		}

		@Override
		public View getView(int position, View convertview, ViewGroup parent) {
			View rowView = convertview;
			if(rowView == null){
				rowView = context.getLayoutInflater().inflate(
						viewID, parent, false);
			}
			TextView txtTitle = (TextView) rowView.findViewById(R.id.txtName);
			TextView txtTimes = (TextView) rowView.findViewById(R.id.txtTime);
			ImageView imageView = (ImageView) rowView.findViewById(R.id.imgTmp);
			// imageView.setImageResource(imageId[position]);
			// FileInputStream currentFile = getFile(getDirectoryPicture(),
			// listProject.get(position).getPicture());
			// Bitmap bitmapFile = BitmapFactory.decodeStream(currentFile);
			// imageView.setImageBitmap(bitmapFile);

			txtTitle.setText(listProject.get(position).getName());
			txtTimes.setText(listProject.get(position).getTimes());
			Log.d("getView", "Create complete");
			return rowView;
		}

	}

}


