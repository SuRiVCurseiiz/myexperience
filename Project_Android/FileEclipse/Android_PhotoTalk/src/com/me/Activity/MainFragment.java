package com.me.Activity;

import java.util.Arrays;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.me.phototalk.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment{
	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	public boolean userCheck = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, 
	        ViewGroup container, 
	        Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.splash, container, false);
	    LoginButton authButton = (LoginButton) view.findViewById(R.id.login_button);
	    authButton.setReadPermissions(Arrays.asList("public_profile"));
	    authButton.setFragment(this);
	    return view;
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }	
	};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 uiHelper = new UiLifecycleHelper(getActivity(), callback);
		 uiHelper.onCreate(savedInstanceState);
	}
	
	@SuppressWarnings("deprecation")
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	        Log.i(TAG, "Logged in...");
	       // userInfoTextView.setVisibility(View.VISIBLE);
	     // Request user data and show the results
	        Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(final GraphUser user, final Response response) {
					// TODO Auto-generated method stub
					Log.d("onCompleted MainFragment", "User = "+user.getName());
					if (user != null) {
	                    // Display the parsed user info
	                    //userInfoTextView.setText(buildUserInfoDisplay(user));
						Intent intent = new Intent(getActivity(),StartUpActivity.class);
						intent.putExtra("UserID", user.getId());
						getActivity().startActivity(intent);
						
						
	                }
				}

	        });
	        
	    } else if (state.isClosed()) {
	    	session.closeAndClearTokenInformation();
	    	//userInfoTextView.setVisibility(View.INVISIBLE);
	        Log.i(TAG, "Logged out...");  
	        
	    }
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    Session session = Session.getActiveSession();
	    if (session != null &&
	           (session.isOpened() || session.isClosed()) ) {
	        onSessionStateChange(session, session.getState(), null);
	    }
	    uiHelper.onResume();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
	
}
