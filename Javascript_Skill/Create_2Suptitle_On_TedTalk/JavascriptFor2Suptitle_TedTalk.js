var ContrainerClass="player__container"

var TranscriptClass="talk-transcript__fragment--current"
var newTitle = "";
var Contrainer = document.getElementsByClassName(ContrainerClass);
var Transcript = document.getElementsByClassName(TranscriptClass);
var newcontent = document.createElement('div');
newcontent.id = "Subtitle2";
newcontent.style.cssText = 'position: inherit;  color: WHITE; z-index: 9999; bottom:0px; text-align: center; background-color:black;';
Contrainer[0].appendChild(newcontent)
var span = document.querySelector('span.controls__time.controls__time--elapsed');

// This may be deprecated.
span.addEventListener('DOMCharacterDataModified', function() {
       
});

// You may use MutationObserver instead.
var mutateObserver = new MutationObserver(function(records) {
  Transcript = document.getElementsByClassName(TranscriptClass);
	if(Transcript){
		newcontent.innerHTML = Transcript[0].innerHTML;
	}
});
mutateObserver.observe(span, {
  childList: true,                                 // capture child add/remove on target element.
  characterData: true,                     // capture text changes on target element
  subtree: true,                                   // capture childs changes too
  characterDataOldValue: true  // keep of prev value
});
